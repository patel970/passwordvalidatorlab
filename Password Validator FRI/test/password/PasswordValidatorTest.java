package password;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * 
 * @author Dhruvil Patel
 *
 */

public class PasswordValidatorTest {
	
	@Test
	public void testHasValidCaseCharsRegular() {
		assertTrue( "Invalid case chars", PasswordValidator.hasValidCaseChars("fdfdRfff")  );
	}
	
	@Test
	public void testHasValidCaseCharsBoundaryIn() {
		assertTrue( "Invalid case chars", PasswordValidator.hasValidCaseChars("Aa")  );
	}
	
	@Test
	public void testHasValidCaseCharsException() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("12345") );
	}
	
	
	@Test
	public void testHasValidCaseCharsExceptionEmpty() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("") );
	}
	
	/*
	@Test
	public void testHasValidCaseCharsExceptionBoundaryOutUpper() {
		assertFalse("Invalid case chars", PasswordValidator.hasValidCaseChars("A") );
	}
	*/
	
	//////

	@Test
	public void testIsValidLengthRegular() {
		boolean isValidLength = PasswordValidator.isValidLength("HelloWorld");
		assertTrue( "Invalid Password Length", isValidLength  );
		}
	
	@Test
	public void testIsValidLengthException() {
		boolean isValidLength  = PasswordValidator.isValidLength("Hello");
		assertFalse( "Invalid Password Length", isValidLength  );
		}
	
	@Test
	public void testIsValidLengthBoundaryOut() {
	
		boolean isValidLength  = PasswordValidator.isValidLength("Hello2");
		assertFalse( "Invalid Password Length", isValidLength  );
		
		}
	
	@Test
	public void testIsValidLengthBoundaryIn() {
	
		boolean isValidLength  = PasswordValidator.isValidLength("Hello123");
		assertTrue( "Invalid Password Length", isValidLength  );
		
		}
	
	
	
	
	
}
